# Setup Script #

This is a script I use to setup a throw-away dev instance. It may or may not be a useful example for anyone else.

```
curl https://bitbucket.org/Procrastes/setup/raw/master/setup | bash
```
or for Digital Ocean
```
#cloud-config

runcmd:
  -  curl https://bitbucket.org/Procrastes/setup/raw/master/setup | bash
```
